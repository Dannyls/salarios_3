# ___Programa para calcular salario
# ___Author___ = "Danny Lima"
# ___Email___ = "danny.lima@unl.edu.ec

hrs = float(input("Ingrese el número de horas trabajadas\n"))
trfa = float(input("Ingrese el valor de la tarifa\n"))
sal = (hrs * trfa)
if hrs > 40:
    hrsx = hrs - 40
    pagx = (trfa * 1.5) * hrsx
    sal = (hrs - hrsx) * trfa
    pagt = pagx + sal
    print("Salario con horas extras:\n ", pagt)
elif hrs <= 40:
    print("Salario sin horas extras es: \n", sal)